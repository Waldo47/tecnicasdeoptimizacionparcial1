﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfectionSimulation
{
    class World
    {
        Pen pen = new Pen(Color.Black);

        private Random rnd = new Random();

        private const int width = 300;
        private const int height = 300;
        private Size size = new Size(width, height);
        private List<GameObject>[,] objects = new List<GameObject>[height, width];

        public IEnumerable<GameObject> GameObjects {
            get
            {
                List<GameObject> list = new List<GameObject>();
                for (int i = 0; i < width; i++)
                {
                    for (int x = 0; x < height; x++)
                    {
                        if (objects[i,x]!=null)
                        {
                            list.AddRange(objects[i, x]);
                        }
                        
                    }
                }

                return list;
            }
        }

        public int Width { get { return width; } }
        public int Height { get { return height; } }

        public Point Center { get { return new Point(width / 2, height / 2); } }

        public bool IsInside(Point p)
        {
            return p.X >= 0 && p.X < width
                && p.Y >= 0 && p.Y < height;
        }

        public Point RandomPoint()
        {
            return new Point(rnd.Next(width), rnd.Next(height));
        }

        public float Random()
        {
            return (float)rnd.NextDouble();
        }

        public int Random(int min, int max)
        {
            return rnd.Next(min, max);
        }

        public void AddPerson(GameObject obj)
        {
            
            List<GameObject> listInside = GetListInside(obj.Position);
            if (listInside == null)
            {
                listInside = InitializeListInside(obj.Position);
            }

            listInside.Add(obj);
        }

        List<GameObject> GetListInside(Point position)
        {
            int x = Mod(position.X, width);
            int y = Mod(position.Y, height);

            return objects[x, y];
        }
    
        List<GameObject> InitializeListInside(Point position)
        {
            List<GameObject> listInside = new List<GameObject>();
            int x = Mod(position.X, width);
            int y = Mod(position.Y, height);
            objects[x, y] = listInside;

            return listInside;
        }


        public void Remove(GameObject obj)
        {
            List<GameObject> listInside = GetListInside(obj.Position);
            if (listInside != null)
            {
                listInside.Remove(obj);
            }

        }

        public void Update()
        {
            foreach (GameObject obj in GameObjects)
            {
                Point lastPosition = obj.Position;
                obj.InternalUpdateOn(this);
                obj.Position = Mod(obj.Position, size);

                if (lastPosition != obj.Position)
                {
                    GetListInside(lastPosition).Remove(obj);
                    AddPerson(obj);
                }
            }
        }

        public void DrawOn(Graphics graphics)
        {
            graphics.FillRectangle(Brushes.Black, 0, 0, width, height);

            foreach (GameObject obj in GameObjects)
            {

                pen.Color = obj.Color;
                graphics.FillRectangle(pen.Brush, obj.Bounds);
            }
        }

        public double Dist(Point a, Point b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        public double Dist(int x1, int y1, int x2, int y2)
        {
            return Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
        }

        // http://stackoverflow.com/a/10065670/4357302
        private static int Mod(int a, int n)
        {
            int result = a % n;
            if ((a < 0 && n > 0) || (a > 0 && n < 0))
                result += n;
            return result;
        }
        private static Point Mod(Point p, Size s)
        {
            return new Point(Mod(p.X, s.Width), Mod(p.Y, s.Height));
        }

        public IEnumerable<GameObject> ObjectsAt(Point pos)
        {
            return objects[pos.X,pos.Y];
        }

    }
}
